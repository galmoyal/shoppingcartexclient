import React from "react";

import "./Footer.scss";

function Footer({ children, ...props }) {
  return (
    <footer className="Footer bg-dark text-white mt-5" {...props}>
      &copy; {new Date().getFullYear()} shopping cart project by gal moyal and idan hazan
    </footer>
  );
}

export default Footer;
