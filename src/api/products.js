import img1 from "../img/img1.jpeg";
import img2 from "../img/img2.jpeg";
import img3 from "../img/img3.jpeg";
import img4 from "../img/img4.jpeg";
import img5 from "../img/img5.jpeg";
import img6 from "../img/img6.jpeg";

function getProducts() {
  return new Promise((res, rej) => {
    $.ajax({
      url: "http://localhost:3001/products",
      type: "GET",
      contentType: "application/json",
      dataType: "json",
      success: function (result) {
        for (const item of result) {
          item.id = item._id;
          switch (item.img) {
            case 1:
              item.img = img1;
              break;
            case 2:
              item.img = img2;
              break;
            case 3:
              item.img = img3;
              break;
            case 4:
              item.img = img4;
              break;
            case 5:
              item.img = img5;
              break;
            case 6:
              item.img = img6;
              break;
          }
        }
        res(result);
      },
    });
    // res(products);
  });
}
function checkOut(cartItems) {
  return new Promise((res, rej) => {
    $.ajax({
      url: "http://localhost:3001/cart/checkout",
      type: "POST",
      data: JSON.stringify(cartItems),
      contentType: "application/json",
      dataType: "json",
      success: function (result) {
        res(result);
      },
    });
  });
}
function createProduct(product) {
  return new Promise((res, rej) => {
    return res(product);
    $.ajax({
      url: "http://localhost:3001/products",
      type: "POST",
      data: JSON.stringify(product),
      contentType: "application/json",
      dataType: "json",
      success: function (result) {
        res(result);
      },
    });
  });
}
export { getProducts, checkOut, createProduct };
